from copy import deepcopy

from custom_classes import Schedule, get_min_start_time_dict, get_max_start_time_dict, get_time_before_class, \
    get_time_after_class, sort_classes_into_days, NotValidScheduleException


def merge(this: Schedule, other: Schedule):
    """
    1. get best day from this
    2. from other get day with:
        no repeating classes
        least repeating classes
    3. remove repeating classes
    4. calculate best other day
    5. fill rest of the schedule with rest of the classes
    """

    best_days, other_best_day, other_classes, this_classes = get_days_and_classes(other, this)
    error_count = 0

    while True:
        try:
            new_schedule = Schedule(this.data, False)
            for best in best_days:
                new_schedule.add_existing_classes_to_schedule(this_classes[best])
            new_schedule.add_existing_classes_to_schedule(other_classes[other_best_day])
            new_schedule.add_classes_to_existing_schedule()

            return new_schedule
        except NotValidScheduleException:
            error_count += 1
            continue
        except IndexError:
            error_count += 1
            continue


def get_days_and_classes(other, this):
    this_classes = sort_classes_into_days(deepcopy(this.classes))
    other_classes = sort_classes_into_days(deepcopy(other.classes))
    best_days = get_best_day(this)

    repetition_rang_dict, indexes_to_remove = get_repetition_ranking(best_days, other_classes, this_classes)

    for key in other_classes.keys():
        for j in range(len(indexes_to_remove[key]) - 1, -1, -1):
            del other_classes[key][indexes_to_remove[key][j]]

    time_rang_dict = get_time_ranking(other.classes)
    other_best_day = max(time_rang_dict, key=time_rang_dict.get)
    while other_best_day in best_days:
        del time_rang_dict[other_best_day]
        other_best_day = max(time_rang_dict, key=time_rang_dict.get)

    return best_days, other_best_day, other_classes, this_classes


def get_repetition_ranking(best_days, other_classes, this_classes):
    indexes_to_remove = {
        'MON': [],
        'TUE': [],
        'WED': [],
        'THU': [],
        'FRI': []
    }

    rang_dict = {
        'MON': 0,
        'TUE': 0,
        'WED': 0,
        'THU': 0,
        'FRI': 0
    }

    this_classes_str = []
    for day in best_days:
        for c in this_classes[day]:
            this_classes_str.append(f'{c.name} - {c.type}')

    for day in rang_dict.keys():
        for i, c in enumerate(other_classes[day]):
            if f'{c.name} - {c.type}' in this_classes_str:
                indexes_to_remove[day].append(i)

    return rang_dict, indexes_to_remove


def get_best_day(s: Schedule):
    rang_dict = get_time_ranking(s.classes)

    best_days = []
    for i in range(4):
        best_day = max(rang_dict, key=lambda x: rang_dict[x])
        del rang_dict[best_day]
        best_days.append(best_day)

    return best_days


def get_time_ranking(classes):
    min_start_time = get_min_start_time_dict(classes)
    max_start_time = get_max_start_time_dict(classes)
    rang_dict = {
        'MON': 0,
        'TUE': 0,
        'WED': 0,
        'THU': 0,
        'FRI': 0
    }

    for room in ('A', 'B', 'C', 'D', 'E'):
        for day in rang_dict.keys():
            if not (min_start_time[room][day].hour == 23 and min_start_time[room][day].minute == 59):
                rang_dict[day] += get_time_before_class(day, min_start_time, room)
            if not (max_start_time[room][day].hour == 0 and max_start_time[room][day].minute == 0):
                rang_dict[day] += get_time_after_class(day, max_start_time, room)
    return rang_dict
