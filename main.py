from custom_classes import function
from genetic_algorithm import evolve
from load import load, load_first_generation, create_first_generation


def run_genetic_algorithm(generation, iterations):
    for i in range(iterations):
        generation = evolve(generation)

    return max(generation, key=lambda x: x.get_fitness())


def main():
    data = load()
    generation_count = 15
    # create_first_generation(generation_count, data)
    iterations = 10
    generation = load_first_generation(generation_count)
    print(f'First: {function(max(generation, key=lambda x: x.get_fitness()))}')
    best = run_genetic_algorithm(generation, iterations)

    print(f"Best: {function(best)}")
    print(best)


if __name__ == '__main__':
    main()