import pickle

from custom_classes import Data, Class, Schedule, function


def load():
    data = Data()
    with open("data_timetable.txt") as file:
        for line in file.readlines():
            words = line.strip().split(":")
            if len(words) > 1:
                if words[0] == 'rooms':
                    data.add_classrooms([room.strip() for room in words[-1].split(',')])
                else:
                    continue
            else:
                words = words[0].split(',')
                data.add_class(Class(words[0].split(" - ")[0], words[0].split(" - ")[1], int(words[1]), int(words[2])))
    return data


def create_first_generation(generation_count, data):
    for i in range(generation_count):
        s = Schedule(data)
        with open(f'schedule_{i + 1}.pkl', 'wb') as f:
            pickle.dump(s, f)


def load_first_generation(generation_count):
    schedules = []
    for i in range(generation_count):
        with open(f'schedule_{i + 1}.pkl', 'rb') as f:
            s = pickle.load(f)
            s._fitness = function(s)
            schedules.append(s)
    return schedules
