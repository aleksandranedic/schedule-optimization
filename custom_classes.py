import datetime
import random


class NotValidScheduleException(Exception):
    pass


class Class:
    def __init__(self, name, class_type, duration, semester, meeting_time=None, room=None):
        self._name = name
        self._class_type = class_type
        self._duration = duration
        self._semester = semester
        self._meeting_time = meeting_time
        self._room = room

    def __eq__(self, other):
        if self._name == other.name and self._room == other.room and self._class_type == other.class_type and self._meeting_time == other.meeting_time:
            return True
        else:
            return False

    @property
    def name(self):
        return self._name

    @property
    def type(self):
        return self._class_type

    @property
    def semester(self):
        return self._semester

    @property
    def class_type(self):
        return self._class_type

    @property
    def duration(self):
        return self._duration

    @property
    def meeting_time(self):
        return self._meeting_time

    @property
    def room(self):
        return self._room

    def set_meeting_time(self, time):
        self._meeting_time = time

    def set_room(self, room):
        self._room = room

    def get_meeting_time(self):
        return self._meeting_time

    def __str__(self):
        if self._meeting_time and self._room:
            return f"{self._name} - {self._class_type}\n\tDuration: {self._duration}\n\tSemester: {self._semester}\n" \
                   f"\tTime: {self._meeting_time.start_time} - {self.meeting_time.get_end_time()}\n\tRoom: {self._room} "
        return f"{self._name} - {self._class_type}\tDuration: {self._duration}\tSemester: {self._semester}"

    def are_same_student_group(self, other):
        if self._class_type == other.class_type and self._semester == other.semester:
            return True
        elif self._semester == other.semester and (
                self._class_type == "Predavanje" or other.class_type == "Predavanje"):
            return True
        return False

    def are_at_same_time(self, other):
        return self.meeting_time.are_at_same_time(other.meeting_time)

    def are_in_same_room(self, other):
        return self.room == other.room


class Semester:
    def __init__(self, semester_id):
        self._semester_id = semester_id
        self._classes = []

    @property
    def classes(self):
        return self._classes

    def add_class(self, c):
        self._classes.append(c)


class Classroom:
    def __init__(self, classroom_id):
        self._id = classroom_id

    @property
    def id(self):
        return self._id

    def __str__(self):
        return self._id


class MeetingTime:
    def __init__(self, start_time, duration, day):
        self.start_time = start_time  # datetime.time
        self.duration = duration
        self.day = day  # MON, TUE, WED, THU, FRI

    def __eq__(self, other):
        if self.start_time == other.start_time and self.duration == other.duration and self.day == other.day:
            return True
        else:
            return False

    def get_end_time(self):
        return get_end_time_for_datetime(time=self.start_time, duration=self.duration)

    def are_at_same_time(self, other):
        if self.day == other.day:
            if self.start_time <= other.start_time <= self.get_end_time():
                return True
            elif other.start_time <= self.start_time <= other.get_end_time():
                return True
        return False

    @staticmethod
    def get_possible_days():
        return ["MON", "TUE", "WED", "THU", "FRI"]

    @staticmethod
    def get_possible_durations():
        return [30, 60, 90, 120, 180]

    def __str__(self):
        return f"Begining: {self.start_time}; Duration: {self.duration}; Day: {self.day}"


class Data:
    def __init__(self):
        self.classes = []
        self.classrooms = []
        self.semesters = {}
        self.meeting_times = {}
        self.generate_meeting_times()

    def add_class(self, c):
        self.classes.append(c)
        self.update_semesters(c)

    def add_classroom(self, r):
        self.classrooms.append(r)

    def add_classrooms(self, r):
        for room in r:
            self.add_classroom(Classroom(room))

    def update_semesters(self, c: Class):
        key = c.semester
        if key not in self.semesters.keys():
            self.semesters[key] = Semester(key)
        self.semesters[key].add_class(c)

    def generate_meeting_times(self):
        min_time = datetime.time(hour=7, minute=0)
        max_time = datetime.time(hour=19, minute=0)

        for duration in MeetingTime.get_possible_durations():
            self.meeting_times[duration] = []

        current_time = min_time
        while current_time < max_time:
            for day in MeetingTime.get_possible_days():
                for duration in MeetingTime.get_possible_durations():
                    if current_time > get_max_meeting_time_for_duration(duration):
                        continue
                    else:
                        self.meeting_times[duration].append(MeetingTime(current_time, duration, day))

            current_time = update_current_time(current_time, 15)


def get_min_start_time_dict(classes):
    hour = 23
    minute = 59
    min_start_time = get_time_dict(hour, minute)
    for c in classes:
        if c.meeting_time.start_time < min_start_time[c.room.id][c.meeting_time.day]:
            min_start_time[c.room.id][c.meeting_time.day] = c.meeting_time.start_time
    return min_start_time


def get_max_start_time_dict(classes):
    max_start_time = get_time_dict(0, 0)
    for c in classes:
        if c.meeting_time.start_time > max_start_time[c.room.id][c.meeting_time.day]:
            max_start_time[c.room.id][c.meeting_time.day] = c.meeting_time.start_time
    return max_start_time


def get_time_before_class(day, min_start_time, room):
    return (min_start_time[room][day].hour - 7) * 60 + min_start_time[room][day].minute


def get_time_after_class(day, max_start_time, room):
    return (19 - max_start_time[room][day].hour) * 60 + max_start_time[room][day].minute


class Schedule:
    def __init__(self, data: Data, init=True):
        self._data = data
        self._classes = []
        self._conflicts = 0
        self._fitness = -1

        if init:
            error_count = 0
            while True:
                try:
                    self.initialize_schedule()
                    break
                except NotValidScheduleException:
                    error_count += 1
                    continue
                except IndexError:
                    error_count += 1
                    continue

    def get_fitness(self):
        return self._fitness

    def get_classes(self):
        return self._classes

    def calculate_fitness(self):
        self._fitness = function(self)

    def get_day_in_room(self, room, day):
        classes = sort_classes_into_classrooms(self._classes)
        classes[room] = sort_classes_into_days(classes[room])
        return classes[room][day]

    def __str__(self):
        classes = sort_classes_into_classrooms(self._classes)
        for key in classes.keys():
            classes[key] = sort_classes_into_days(classes[key])
        string = ''

        for room in classes.keys():
            string += f'\n\n\t\t{room}\n'
            for day in classes[room].keys():
                string += f'\n\t{day}\n'
                classes[room][day] = sorted(classes[room][day], key=lambda x: x.meeting_time.start_time)
                for c in classes[room][day]:
                    string += str(c) + '\n'
        return string

    def print(self, days=None, rooms=None):
        classes = sort_classes_into_classrooms(self._classes)
        for key in classes.keys():
            classes[key] = sort_classes_into_days(classes[key])
        string = ''
        for r in classes.keys():
            if rooms:
                if r not in rooms:
                    continue
            string += f'\n\n\t\t{r}\n'
            for d in classes[r].keys():
                if days:
                    if d not in days:
                        continue
                string += f'\n\t{d}\n'
                classes[r][d] = sorted(classes[r][d], key=lambda x: x.meeting_time.start_time)
                for c in classes[r][d]:
                    string += str(c) + '\n'
        print(string)

    def initialize_schedule(self):
        self._classes = []
        self._conflicts = 0
        all_meeting_times = self.get_all_meeting_times()

        for c in self._data.classes:
            room = self.initialize_room()
            c.set_room(room)
            all_meeting_times[room.id], time = self.initialize_time(c, possible_time=all_meeting_times[room.id])
            c.set_meeting_time(time)
            self._classes.append(c)

        self.count_conflicts()
        self._fitness = function(self)

    def initialize_room(self):
        room = random.choice(self._data.classrooms)
        return room

    def add_existing_classes_to_schedule(self, classes):
        for c in classes:
            self._classes.append(c)

    def get_all_meeting_times_for_existing_schedule(self):
        all_meeting_times = self.get_all_meeting_times()
        for c in self._classes:
            room = c.room.id
            time = c.meeting_time
            day = time.day

            all_meeting_times[room] = self.remove_time_from_possible_time(all_meeting_times[room], day, time)

        return all_meeting_times

    def add_classes_to_existing_schedule(self):
        classes_to_add = self.get_classes_to_add()
        all_meeting_times = self.get_all_meeting_times_for_existing_schedule()

        for c in classes_to_add:
            room = self.initialize_room()
            c.set_room(room)
            all_meeting_times[room.id], time = self.initialize_time(c, possible_time=all_meeting_times[room.id])
            c.set_meeting_time(time)
            self._classes.append(c)

    def get_classes_to_add(self):
        classes_to_add = []

        class_names = []
        for c in self._classes:
            class_names.append((c.name, c.type))

        for i, every in enumerate(self._data.classes):
            if (every.name, every.type) not in class_names:
                classes_to_add.append(every)

        return classes_to_add

    @staticmethod
    def initialize_time(c, possible_time):

        days = list(possible_time.keys())

        day = random.choice(days)
        choices = sort_times_into_durations(possible_time[day])[c.duration]

        error_counter = 0

        while True:
            time = random.choice(choices)
            if is_valid_time(c, choices, time):
                break
            else:
                day = random.choice(days)
                choices = sort_times_into_durations(possible_time[day])[c.duration]
                error_counter += 1
                if error_counter > 10000:
                    raise NotValidScheduleException

        possible_time = Schedule.remove_time_from_possible_time(possible_time, day, time)

        return possible_time, time

    @staticmethod
    def remove_time_from_possible_time(possible_time, day, time):
        indexes = []
        next_start_time = update_current_time(time.get_end_time(), minutes=15)
        for i, t in enumerate(possible_time[day]):
            if time.start_time <= t.start_time <= next_start_time:
                indexes.append(i)
        for i in range(len(indexes) - 1, -1, -1):
            del possible_time[day][indexes[i]]
        return possible_time

    def count_conflicts(self):
        self._conflicts = 0
        for i in range(len(self._classes)):
            for j in range(i + 1, len(self._classes)):
                this_class = self._classes[i]
                other_class = self._classes[j]
                if this_class.room.id == other_class.room.id:
                    if this_class.are_at_same_time(other_class):
                        self._conflicts += 1

    def count_conflicts_for_day(self, days):
        conflicts = 0
        for i in range(len(self._classes)):
            for j in range(i + 1, len(self._classes)):
                this_class = self._classes[i]
                other_class = self._classes[j]
                if this_class.meeting_time.day in days and other_class.meeting_time.day:
                    if this_class.room.id == other_class.room.id:
                        if this_class.are_at_same_time(other_class):
                            conflicts += 1
        return conflicts

    def get_sum_of_time_before_classes(self):
        min_start_time = get_min_start_time_dict(self._classes)

        sum_of_times = 0
        for room in min_start_time.keys():
            for day in min_start_time[room].keys():

                if not (min_start_time[room][day].hour == 23 and min_start_time[room][day].minute == 59):
                    sum_of_times += get_time_before_class(day, min_start_time, room)
                else:
                    sum_of_times += 6 * 60
        return sum_of_times

    def get_sum_of_time_after_classes(self):
        max_start_time = get_max_start_time_dict(self._classes)

        sum_of_times = 0
        for room in max_start_time.keys():
            for day in max_start_time[room].keys():
                if not (max_start_time[room][day].hour == 0 and max_start_time[room][day].minute == 0):
                    sum_of_times += get_time_after_class(day, max_start_time, room)
                else:
                    sum_of_times += 6 * 60
        return sum_of_times

    def get_all_meeting_times(self):
        all_meeting_times = {
            'A': [],
            'B': [],
            'C': [],
            'D': [],
            'E': []
        }
        list_of_all_times = []
        for value in self._data.meeting_times.values():
            list_of_all_times += value
        for key in all_meeting_times.keys():
            all_meeting_times[key] = sort_times_into_days(list_of_all_times)
        return all_meeting_times

    @property
    def conflicts(self):
        return self._conflicts

    @property
    def classes(self):
        return self._classes

    @property
    def data(self):
        return self._data


def is_valid_time(c, choices, time):
    number = c.duration // 15 + 1

    times = [0] * number
    compare_time = time.start_time
    for i in range(number):
        for choice in choices:
            if compare_time == choice.start_time:
                times[i] = 1
                break
        compare_time = update_current_time(compare_time, 15)

    if sum(times) < number:
        return False
    else:
        return True


def is_group_taken(time, group_times):
    for t in group_times:
        if time.are_at_same_time(t):
            return True
    else:
        return False


def get_time_dict_with_days_of_the_week(hour, minute):
    return {
        'MON': datetime.time(minute=minute, hour=hour),
        'TUE': datetime.time(minute=minute, hour=hour),
        'WED': datetime.time(minute=minute, hour=hour),
        'THU': datetime.time(minute=minute, hour=hour),
        'FRI': datetime.time(minute=minute, hour=hour)}


def get_time_dict(hour, minute):
    min_start_time = {
        "A": get_time_dict_with_days_of_the_week(hour, minute),
        "B": get_time_dict_with_days_of_the_week(hour, minute),
        "C": get_time_dict_with_days_of_the_week(hour, minute),
        "D": get_time_dict_with_days_of_the_week(hour, minute),
        "E": get_time_dict_with_days_of_the_week(hour, minute)}
    return min_start_time


def get_end_time_for_datetime(time, duration):
    if duration < 60 and time.minute < 60 - duration:
        return datetime.time(hour=time.hour, minute=time.minute + duration)
    elif duration < 60:
        return datetime.time(hour=time.hour + 1, minute=time.minute - duration)
    elif duration == 90:
        if time.minute < 30:
            return datetime.time(hour=time.hour + 1, minute=time.minute + 30)
        else:
            return datetime.time(hour=time.hour + 1, minute=time.minute - 30)
    else:
        return datetime.time(hour=time.hour + (duration // 60), minute=time.minute)


def get_max_meeting_time_for_duration(duration):
    if duration == 180:
        return datetime.time(16, 0)
    elif duration == 120:
        return datetime.time(17, 0)
    elif duration == 90:
        return datetime.time(17, 30)
    elif duration == 60:
        return datetime.time(18, 0)
    elif duration == 30:
        return datetime.time(18, 30)
    else:
        raise Exception


def get_hours_and_minutes_from_duration(duration):
    if duration < 60:
        return 0, duration
    elif duration == 60:
        return 1, 0
    elif duration == 90:
        return 1, 30
    elif duration == 120:
        return 2, 0
    elif duration == 180:
        return 3, 0


def update_current_time(current_time: datetime.time, minutes, hours=0):
    if current_time.minute < (60 - minutes):
        return datetime.time(hour=current_time.hour + hours, minute=current_time.minute + minutes)
    else:
        return datetime.time(hour=current_time.hour + hours + 1, minute=0)


def sort_times_into_days(times):
    result = {
        'MON': [],
        'TUE': [],
        'WED': [],
        'THU': [],
        'FRI': []
    }
    for t in times:
        result[t.day].append(t)
    return result


def sort_classes_into_days(list_of_classes):
    classes = {
        'MON': [],
        'TUE': [],
        'WED': [],
        'THU': [],
        'FRI': []
    }
    for c in list_of_classes:
        classes[c.meeting_time.day].append(c)
    return classes


def sort_classes_into_classrooms(list_of_classes):
    classes = {
        'A': [],
        'B': [],
        'C': [],
        'D': [],
        'E': []
    }
    for c in list_of_classes:
        classes[c.room.id].append(c)
    return classes


def sort_classes_into_durations(list_of_classes):
    classes = {
        30: [],
        60: [],
        90: [],
        120: [],
        180: []
    }
    for c in list_of_classes:
        classes[c.duration].append(c)
    return classes


def sort_times_into_durations(list_of_times):
    times = {
        30: [],
        60: [],
        90: [],
        120: [],
        180: []
    }
    for t in list_of_times:
        times[t.duration].append(t)
    return times


def function(schedule):
    return schedule.get_sum_of_time_before_classes() * schedule.get_sum_of_time_after_classes()
