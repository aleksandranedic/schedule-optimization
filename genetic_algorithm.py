import datetime
import random
from copy import deepcopy

from custom_classes import Schedule, Class, MeetingTime
from merge import merge

NUMBER_OF_BEST_SCHEDULES = 3
POPULATION_SIZE = 15
MUTATION_RATE = 0.6
ROOMS = ["A", "B", "C", "D", "E"]
DAYS = ["MON", "TUE", "WED", "THU", "FRI"]


def evolve(population: list):
    return _mutate_population(_crossover_population(population))


def _crossover_population(old_population):
    for schedule in old_population:
        schedule.calculate_fitness()
    old_population.sort(key=lambda x: x.get_fitness(), reverse=True)

    new_population = []
    for i in range(NUMBER_OF_BEST_SCHEDULES):
        new_population.append(old_population[i])
    new_population_num = NUMBER_OF_BEST_SCHEDULES
    while new_population_num < POPULATION_SIZE:
        schedule1, schedule2 = _select_individual(old_population)
        new_population.append(merge(schedule1, schedule2))
        new_population[-1].calculate_fitness()
        new_population_num += 1
    return _elitism(new_population, old_population)


def _elitism(new_population, old_population):
    population = new_population + old_population
    population.sort(key=lambda x: x.get_fitness(), reverse=True)
    new_population.sort(key=lambda x: x.get_fitness(), reverse=True)
    new_pop = population[0:NUMBER_OF_BEST_SCHEDULES]
    for i in range(POPULATION_SIZE):
        if new_population[i] not in new_pop:
            new_pop.append(new_population[i])
    return new_pop


def _mutate_population(population):
    for i in range(POPULATION_SIZE):
        population[i] = _mutate_schedule(population[i])
    return population


def _mutate_schedule(schedule):
    if MUTATION_RATE > random.uniform(0, 1):
        new_schedule = Schedule(schedule.data, False)
        for room in ROOMS:
            for day in DAYS:
                new_schedule.classes.extend(_mutate_day_in_room(schedule.get_day_in_room(room, day)))
        new_schedule.calculate_fitness()
        return new_schedule
    return schedule


def _mutate_day_in_room(classes_for_day_in_room: list):
    classes_for_day_in_room.sort(key=lambda x: x.get_meeting_time().start_time, reverse=False)
    for i in range(len(classes_for_day_in_room) - 1):
        for j in range(i + 1, len(classes_for_day_in_room)):
            time_difference = _get_time_difference(classes_for_day_in_room[i], classes_for_day_in_room[j])
            if abs(time_difference) > 15:
                classes_for_day_in_room[j] = _move_earlier(int(time_difference), classes_for_day_in_room[j])
            elif time_difference == 0:
                classes_for_day_in_room[j] = _move_earlier(-15, classes_for_day_in_room[j])
            elif time_difference == - 15:
                classes_for_day_in_room[j] = _move_earlier(-30, classes_for_day_in_room[j])
            break
    new_classes = []
    for one_cl in classes_for_day_in_room:
        new_classes.append(
            Class(one_cl.name, one_cl.class_type, one_cl.duration, one_cl.semester, deepcopy(one_cl.meeting_time),
                  deepcopy(one_cl.room)))
    return new_classes


def _get_time_difference(class1, class2):
    if class1.get_meeting_time().start_time < class2.get_meeting_time().start_time:
        ending_time = class1.get_meeting_time().get_end_time()
        starting_time = class2.get_meeting_time().start_time
    else:
        ending_time = class2.get_meeting_time().get_end_time()
        starting_time = class1.get_meeting_time().start_time

    date_time_end = datetime.datetime.combine(datetime.date.today(), ending_time)
    date_time_start = datetime.datetime.combine(datetime.date.today(), starting_time)
    date_time_difference = date_time_start - date_time_end
    difference_in_minutes = date_time_difference.total_seconds() / 60
    return difference_in_minutes


def _move_earlier(time, one_class):
    time = time - 15
    hours = abs(time) // 60
    minutes = abs(time) % 60
    if time < 0:
        hours = hours * (-1)
        minutes = minutes * (-1)

    new_meeting_time = MeetingTime(change_time(one_class.get_meeting_time().start_time, minutes, hours),
                                   one_class.get_meeting_time().duration, one_class.get_meeting_time().day)
    new_ending_time = new_meeting_time.get_end_time()
    if new_ending_time >= datetime.time(hour=19, minute=00):
        return deepcopy(one_class)
    else:
        new_class = Class(one_class.name, one_class.class_type, one_class.duration, one_class.semester,
                          new_meeting_time, one_class.room)
        return new_class


def _select_individual(population: list):
    population.sort(key=lambda x: x.get_fitness() * random.uniform(0, 1), reverse=True)
    return population[0], population[1]


def change_time(time: datetime.time, minutes, hours):
    day = datetime.datetime(2020, 5, 7, hour=time.hour, minute=time.minute)
    d = day - datetime.timedelta(hours=hours, minutes=minutes)
    return datetime.time(hour=d.hour, minute=d.minute)
